const api = {
    url: "https://upload.giphy.com/v1/gifs",
    key: "1Ym5QFHtcssvCFVdYBi0X2eLg0uX2R54",
    username: "carbis92"
};
const cameraVideo = document.querySelector("#camera-video");
const gifPreview = document.querySelector(".gif-preview");
const startGif = document.querySelector(".btn-comenzar-gif");
var misGif = [];

async function getStreamAndRecord() {
    let stream = await navigator.mediaDevices.getUserMedia({
        video: true,
        audio: false
    });
    let recorder = new RecordRTCPromisesHandler(stream, {
        type: "gif",
        frameRate: 1,
        quality: 10,
        width: 360,
        hidden: 240
    });

    cameraVideo.srcObject = stream;
    cameraVideo.play();
    recorder.startRecording();

    const sleep = m => new Promise(r => setTimeout(r, m));
    await sleep(1000);

    await recorder.stopRecording();

    let blob = await recorder.getBlob();

    if (blob) {
        saveRecording(blob);
        invokeSaveAsDialog(blob);
    }
}

function saveRecording(blob) {
    let URL = `${api.url}?api_key=${api.key}&username=${api.username}`;
    let data = new FormData();
    let params = {
        method: "POST",
        body: data.append("file", blob, "misGif.gif"),
        mode: "no-cors"
    };

    const fetchResponse = fetch(URL, params)
        .then(response => response.json())
        .then(datos => saveGifOnLS(datos.data.id))
        .catch(error => console.log(error));

    return fetchResponse;
}

function saveGifOnLS(id) {
    if (localStorage.getItem("GifList")) {
        misGif = JSON.parse(localStorage.getItem("GifList"));
        misGif.push(id);
        localStorage.setItem("GifList", JSON.stringify(misGif));
    } else {
        misGif.push(id);
        localStorage.setItem("GifList", JSON.stringify(misGif));
    }
}

startGif.addEventListener("click", getStreamAndRecord);
