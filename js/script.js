// URL y key de la API
const api = {
    url: "https://api.giphy.com/v1/",
    key: "1Ym5QFHtcssvCFVdYBi0X2eLg0uX2R54"
};
const suggestionsWrapper = document.querySelector(".suggestions-wrapper");
const gifsWrapper = document.querySelector(".gridContainer");

async function fetchElements(type, limit, wrapper) {
    try {
        const response = await fetch(
            `${api.url + type}/trending?api_key=${api.key}&limit=${limit}`
        );
        const result = await response.json();
        return appendElements(wrapper, result.data);
    } catch (error) {
        return console.log(error);
    }
}

async function searchService(type, searchParams, limit, wrapper) {
    try {
        const response = await fetch(
            `${api.url + type}/search?q=${searchParams}&api_key=${
                api.key
            }&limit=${limit}`
        );
        const result = await response.json();
        return appendElements(wrapper, result.data);
    } catch (error) {
        return console.log(error);
    }
}

async function getRandomElement(type, container) {
    try {
        const response = await fetch(
            `${api.url + type}/random?api_key=${api.key}`
        );
        const result = await response.json();
        container.src = result.data.images.original.url;
    } catch (error) {
        return console.log(error);
    }
}

function appendElements(wrapper, elements) {
    wrapper.innerHTML = "";
    elements.map((element, index) => {
        const imageWrapper = document.createElement("div");
        const closeButton = document.createElement("img");
        const textNode = document.createElement("p");
        const image = document.createElement("img");
        const button = document.createElement("button");

        closeButton.setAttribute("src", "./assets/button_close.svg");
        closeButton.classList.add("gif-close-button");

        textNode.innerHTML = `#${element.title.replace(/ /g, "")}`;
        textNode.append(closeButton);

        getRandomElement("gifs", image);

        if (wrapper !== gifsWrapper) {
            button.innerHTML = "Ver más...";
            button.classList.add("gif-view-more");
            button.addEventListener("click", () =>
                searchService(
                    "gifs",
                    element.title.toLowerCase(),
                    10,
                    gifsWrapper
                )
            );

            imageWrapper.append(button);
        }

        imageWrapper.classList.add("gif-container");
        imageWrapper.style.animationDelay = `${index * 75}ms`;
        imageWrapper.append(textNode);
        imageWrapper.append(image);
        wrapper.append(imageWrapper);
    });
}

// Aca hacemos el append de las sugerencias
fetchElements("gifs", 4, suggestionsWrapper);

// Aca hacemos el append al gifWrapper
fetchElements("gifs", 10, gifsWrapper);

const gifSearchButton = document.querySelector("#gifSearch");
// const stickerSearchButton = document.querySelector("#stickersSearch");
const inputSearchBar = document.querySelector("#name");

gifSearchButton.addEventListener("click", () => {
    searchService("gifs", inputSearchBar.value, 10, gifsWrapper);
});

// stickerSearchButton.addEventListener("click", () => {
//     searchService("stickers", inputSearchBar.value);
// });

let ThemeBtn = document.querySelector(".theme-btn");
let Body = document.querySelector("body");
function toggleDarkTheme() {
    Body.classList.toggle("dark");
}
ThemeBtn.addEventListener("click", toggleDarkTheme);
